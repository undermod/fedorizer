# Fedorizer

Fedora post installation script to achieve a fully funcional Bspwm setup

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/undermod/fedorizer.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/undermod/fedorizer/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.


---

# LISTA SOFTWARE

## ESSENZIALI:
- bspwm (Window Manager)
- sxhkd (Key deamon)
- alacritty (Terminal)
- polybar (Bar)
- picom (Compositor)
- rofi (Menu)
- dmenu (Alternative Menu)
- dunst (Notification deamon)
- unzip (Extraction tool)
- git (Versioning)

## SHELL
- fish shell
- starship theme

## NETWORK
- NetworkManager (Network Manager)
- libsmbclient (Preinstalled library)

## UTILITY
- xdg-user-dirs (User directory creation tool)
- xsetroot (Window utility)

## REPOSITORY ENABLED
- RPM Fusion Free
- RPM Fusion NonFree

## EYECANDY
- JetBrains Mono Nerd Fonts (Fonts)

** ALL MULTIMEDIA CODEC INSTALLED **

---

###################################

devel_aur_pkg=("gitkraken")

network_aur_pkg=("zoom" "")

eyecandy_pkg=("fontawesome5-free-fonts" "powerline-fonts")

Risorse = tela-icon sweet-cursor dracula-gtk-theme Jetbrains-Nerd-Fonts

######################################################################

PACKAGES 

-ESSENZIALI
Microcode:  	 		intel-ucode ?
-PolicyKit:	    		polkit-gnome

-NETWORK
-Applet: 		    	network-manager-applet
-Samba fs:			    gvfs-smb
-SSH:				    openssh
-Browser:			    firefox
-Chat:			        hexchat
Skype:			        skype (check git)
Rss:				    FeedReader (check git)
-Telegram:			    telegram-desktop
-File:			        wget uget
-FTP:				    filezilla
-Video:			        youtube-dl
-Torrent:			    transmission-gtk
-Remote control         anydesk

-SESSIONE GRAFICA
Accelerazione Video: 	intel-video-accel
-Classe applicazione:	xprop
-Luminosità:		    xbacklight
-Gestione device:		arandr autorandr

-AUDIO:
-Mixer:			        pavucontrol
-Icona taskbar:		    volumeicon

-WINDOW MANAGER
Lockscreen:			    betterlockscreen (check git)
Logout:	    		    clearine (check git)


-SHELL
-Fish:		        	fish
-Prompt:			    starship
-Color ls:			    lsd
Gestore processi:		btop (check git)
-Utilizzo disco:		ncdu

-EDITOR
-Shell Editor:		    vim
-Gui editor:		    mousepad

-FILE MANAGER
-File manager cli:	    Ranger
-File manager gui:	    Thunar
-Gestore volumi:		thunar-volman
-Gestore archivi:		thunar-archive-plugin
-Thumbnail:			    tumbler
-Gestione unità:		udiskie gvfs ntfs-3g xdg-open ?

-BACKUP
-Backup:			    timeshift

UTILITY
-Rar:				    unrar
-Gestore archivi:		xarchiver
-Screenshot:		    flameshot
-Nascondi cursore:	    unclutter-xfixes
-Gestore partizioni:	gparted
-Gestore password:	    keepassxc
-Clipboard:			    parcellite

UFFICIO
-Suite ufficio:		    libreoffice
-Lingua suite:		    libreoffice-langpack-it
-PDF:				    mupdf

DEV
-Confronto:			    meld
-IDE:				    vscode (repo vscode)
-Git Gui:			    git-g
-Python utility		    python3-pip python3-virtualenv


CLOUD
-Nextcluod:			    nextcloud-client
-Dropbox:			    dropbox nautilus-dropbox

EYE CANDY
-Sys info:			    neofetch
-Sys summary:		    inxi
-Immagini term:		    w3m w3m-img
-Wallpaper:			    nitrogen
-Temi:			        lxappearance


MEDIA
-Video player:	    	vlc (rpm fusion) mpv (rpm fusion)
-Music player:		    sayonara (ok in repo) deadbeef (rpm fusion)
-Video editor:		    vidcutter (check git) HandBrake-gui (rpm fusion)
-Nonlinear video editor: olive (rpm fusion)
Spotify:			    spotify-tui spotify-qt (check git)

GRAFICA:
-Image viewer:		    ristretto
-Raster:			    gimp
-Vettoriale:		    inkscape

######################################################################

PROCEDURE

* 1) Modifica Configurazione DNF

sudo nano /etc/dnf/dnf.conf

fastestmirror=True
deltarpm=True
max_parallel_downloads=10

* 2) Update Sistema

sudo dnf update

3) Set Hostname (ozionale)

hostnamectl set-hostname spectre

* 4) Creazione directory utente

sudo dns install xdg-user-dirs
xdg-user-dirs-update

* 5) Attivazione repository terze parti

sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

* 6) Installazione server grafico

sudo dnf install @base-x

* 7) Installazione display manager

sudo dnf install lightdm lightdm-gtk lightdm-gtk-greeter-settings
sudo systemctl enable lightdm
sudo systemctl set-default graphical.target

* 8) Installazione window manager

sudo dnf install bspwm sxhkd polybar alacritty git

* 9) Creare cartella configurazioni ".config" e copiare i dotfile al suo interno in modo da avere già i programmi configurati non appena installati

* 10) Installazione font JetBrains

Copiare ed estrarre zip nella cartella ".fonts" ed aggiornare la cache dei caratteri con "fc-cache -vf"

* 11) Installazione codec multimediali

* 12) Installazione Shell Fish

Aggiungere sbin al PATH
set PATH $PATH /usr/sbin

* 13) Abilitazione repo ed installazione VSCode

sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
printf "[vscode]\nname=packages.microsoft.com\nbaseurl=https://packages.microsoft.com/yumrepos/vscode/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc\nmetadata_expire=1h" | sudo tee -a /etc/yum.repos.d/vscode.repo
sudo dnf install code -y

13) Installazione vari software ed eventuali configurazioni

14) Draculizzare il sistema

*Xresources
Tema gtk
*Fish + Starhip (creare procedura)
*Alacritty (creare procedura)
Vim (creare procedura)
Mousepad (creare procedura)
Ranger (creare procedura) ?
Firefox (Addons - Dracula Theme)
Visual Studio Code (installare estensione Dracula Official)
Telegram-desktop (creare procedura)

15) Reboot sistema


Tips:

unzip -q nome_archivio -d path_file_estratti
