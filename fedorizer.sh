#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Fedora full setup from minimal installation      #
#                                                                   #
#####################################################################   
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#              							                            #
#####################################################################

# --- Dichiarazione iniziale variabili --- #

ver="0.1"
procedura=""

Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
Cyan='\e[0;36m'         # Ciano
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso
BCyan='\e[1;36m'        # Ciano Intenso
 

# --- Funzioni di servizio  --- #

# Intestazione dello script
logo (){
    clear
    echo -e "${BWhite}###################################################################${Color_Off}"
    echo -e "${BWhite}#     FEDORIZER - Fedora post installation utility - Ver. $ver     #${Color_Off}"
    echo -e "${BWhite}###################################################################${Color_Off}"
    echo
}

# Aggiornamento sistema
updade (){
    logo
    echo -e "${BCyan}:::   AGGIORNAMENTO SISTEMA   :::${Color_Off}"
    echo
    sudo dnf upgrade -y
    echo
    echo -e "${BGreen}+++   Aggiornamento sistema eseguito   +++${Color_Off}"
    sleep 0.5
}

# Check dipendenze
check_dipendenze (){
    logo
    echo -e "${BCyan}:::   CHECK DIPENDENZE   :::${Color_Off}"
    echo
    echo -e "${Green}==> Check Unzip  ...${Color_Off}"
    echo
    if sudo dnf list installed | grep git &> /dev/null
    then
        echo
        echo -e "${BGreen}+++   Dipendenza Git Soddisfatta   +++${Color_Off}"
        echo
    else
        echo
        echo -e "${Green}==> Installazione Git  ...${Color_Off}"
        echo
        sudo dnf install git -y
        echo
        echo -e "${BGreen}+++   Installazione Git eseguita con successo   +++${Color_Off}"
        sleep 0.5
    fi
    if sudo dnf list installed | grep unzip &> /dev/null
    then
        echo
        echo -e "${BGreen}+++   Dipendenza Unzip Soddisfatta   +++${Color_Off}"
        echo
    else
        echo
        echo -e "${Green}==> Installazione Unzip  ...${Color_Off}"
        echo
        sudo dnf install unzip -y
        echo
        echo -e "${BGreen}+++   Installazione Unzip eseguita con successo   +++${Color_Off}"
        sleep 0.5
    fi
    #TODO: completare
}

# Riavvio del sistema al termine dell'installazione
riavvio(){
    echo -e "${BGreen}:::   INSTALLAZIONE TERMINATA   :::${Color_Off}"
    echo
    echo -e "${Green}==> Premere un tasto per riavviare${Color_Off}"
    echo
    read input &> /dev/null
    reboot
}

# Installazione software
software (){
    if [[ "$procedura" == "Gnome" ]];
    then
        lista_software=gnome.txt
    fi
    if [[ "$procedura" == "Bspwm" ]];
    then
        lista_software=bspwm.txt
    fi
    if [[ "$procedura" == "I3" ]];
    then
	lista_software=i3.txt    
    fi	    
    logo
    echo -e "${BCyan}:::   INSTALLAZIONE LISTA SOFTWARE   :::${Color_Off}"
    echo
    while read item
    do
        if sudo dnf list installed | grep $item &> /dev/null
        then
            echo
            echo -e "${BGreen}+++   $item già installato   +++${Color_Off}"
            echo
        else
            echo
            echo -e "${Green}==> Installazione $item  ...${Color_Off}"
            echo
            sudo dnf install $item -y
            echo
            echo -e "${BGreen}+++   Installazione $item eseguita con successo   +++${Color_Off}"
            sleep 0.5
        fi
    done < "$lista_software"
}

# --- Funzioni richiamate dallo script --- #

# Disclaimer iniziale
disclaimer (){
    logo
    echo -e "${BYellow}Proseguendo verrà avviata la procedura di post installazione${Color_Off}"
    echo -e "${BYellow}basata sulla configurazione utilizzata da Und3rm0d.${Color_Off}"
    echo
    printf "Si desidera continuare ? (S/N) ==> "
    read input
    if [ "$input" == "n" ] || [ "$input" == "N" ]; then
        exit 0
    fi
    clear
    logo
    echo -e "${BYellow}E' possibile effettuare la procedura di post installazione ${Color_Off}"
    echo -e "${BYellow}per GNOME (G) , Bspwm (B) , I3 (I).${Color_Off}"
    echo
    printf "Digita la tua scelta ? (G/B/I) ==> "
    read input
    if [ "$input" == "G" ] || [ "$input" == "g" ]; then
        procedura="Gnome"
    fi
    if [ "$input" == "B" ] || [ "$input" == "b" ]; then
        procedura="Bspwm"
    fi
    if [ "$input" == "I" ] || [ "$input" == "i" ]; then
	procedutra="I3"
    fi
}

# Riepilogo scelta di post installazione
riepilogo (){
    logo
    if [[ "$procedura" == "" ]];
    then
        echo -e "${BYellow}Non hai effettuato nessuna scelta${Color_Off}"
        echo -e "${BYellow}Premi un tasto per effettuare la tua scelta ... ${Color_Off}"
        read input
        disclaimer
    else
        echo -e "${BYellow}Hai scelto di effettuare la configurazione di $procedura ${Color_Off}"
    fi
    echo
    printf "Si desidera continuare ? (S/N) ==> "
    read input
    if [ "$input" == "n" ] || [ "$input" == "N" ]; then
        exit 0
    fi
}

# Tuning di DNF per renderlo più veloce
tuning_dnf (){
    logo
    echo -e "${BCyan}:::   TUNING DNF   :::${Color_Off}"
    echo
    sudo echo "# Modifiche per incrementare la velocità" >> /etc/dnf/dnf.conf
    echo
    echo -e "${Green}==> Attivazione mirror più veloci ...${Color_Off}"
    echo
    sudo echo "fastestmirror=True" >> /etc/dnf/dnf.conf
    echo
    echo -e "${Green}==> Attivazione deltarpm ...${Color_Off}"
    echo
    sudo echo "deltarpm=True" >> /etc/dnf/dnf.conf
    echo
    echo -e "${Green}==> Attivazione download paralleli ...${Color_Off}"
    echo
    sudo echo "max_parallel_downloads=5" >> /etc/dnf/dnf.conf
    echo
    echo -e "${BGreen}+++   Tuning DNF eseguito con successo   +++${Color_Off}"
    sleep 0.5
}

# Creazione directory utente
user_dirs (){
    logo
    echo -e "${BCyan}:::   CREAZIONE DIRECTORY UTENTE   :::${Color_Off}"
    echo
    echo -e "${Green}==> Installazione utility  ...${Color_Off}"
    echo
    sudo dnf install xdg-user-dirs -y
    echo
    echo -e "${Green}==> Generazione directory utente  ...${Color_Off}"
    echo
    xdg-user-dirs-update
    echo
    echo -e "${BGreen}+++   Creazione directory utente eseguito con successo   +++${Color_Off}"
    sleep 0.5
}

# Attivazione repository terze party
repo (){
    logo
    echo -e "${BCyan}:::   ATTIVAZIONE REPOSITORY TERZE PARTI   :::${Color_Off}"
    echo
    echo -e "${Green}==> Installazione RPM Fusion Free  ...${Color_Off}"
    echo
    sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm -y
    echo
    echo -e "${Green}==> Installazione RPM Fusion Non-Free  ...${Color_Off}"
    echo
    sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm -y
    echo
    echo -e "${Green}==> Importazione chiave Microsoft   ...${Color_Off}"
    echo
    sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
    echo
    echo -e "${Green}==> Abilitazione repo Visual Studio Code   ...${Color_Off}"
    echo
    printf "[vscode]\nname=packages.microsoft.com\nbaseurl=https://packages.microsoft.com/yumrepos/vscode/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc\nmetadata_expire=1h" | sudo tee -a /etc/yum.repos.d/vscode.repo
    echo
    echo
    echo -e "${Green}==> Abilitazione repo Anydesk   ...${Color_Off}"
    echo
    printf "[anydesk]\nname=AnyDesk Fedora - stable\nbaseurl=http://rpm.anydesk.com/fedora/\$basearch/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://keys.anydesk.com/repos/RPM-GPG-KEY" | sudo tee -a /etc/yum.repos.d/AnyDesk-Fedora.repo
    sudo dnf --releasever=32 install pangox-compat.x86_64
    echo
    echo -e "${BGreen}+++   Abilitazione repository eseguita con successo   +++${Color_Off}"
    sleep 0.5
}

# Installazione server grafico
server_grafico (){
    logo
    echo -e "${BCyan}:::   INSTALLAZIONE SERVER GRAFICO   :::${Color_Off}"
    echo
    echo -e "${Green}==> Installazione Xorg  ...${Color_Off}"
    echo
    sudo dnf install @base-x -y
    echo
    echo -e "${BGreen}+++   Installazione server grafico eseguita con successo   +++${Color_Off}"
    sleep 0.5
}

# Installazione display manager
display_manager (){
    logo
    echo -e "${BCyan}:::   INSTALLAZIONE DISPLAY MANAGER   :::${Color_Off}"
    echo
    echo -e "${Green}==> Installazione Lightdm e Gtk greeter  ...${Color_Off}"
    echo
    sudo dnf install lightdm lightdm-gtk lightdm-gtk-greeter-settings -y
    echo
    echo -e "${Green}==> Attivazione Lightdm  ...${Color_Off}"
    echo
    sudo systemctl enable lightdm
    sudo systemctl set-default graphical.target
    echo
    echo -e "${BGreen}+++   Installazione display manager eseguita con successo   +++${Color_Off}"
    sleep 0.5
}

# Installazione bspwm window manager
bspwm_window_manager (){
    logo
    echo -e "${BCyan}:::   INSTALLAZIONE WINDOW MANAGER   :::${Color_Off}"
    echo
    echo -e "${Green}==> Installazione Bspwm  ...${Color_Off}"
    echo
    sudo dnf install bspwm sxhkd polybar alacritty xsetroot picom dunst dmenu rofi -y
    echo
    echo -e "${BGreen}+++   Installazione window manager eseguita con successo   +++${Color_Off}"
    sleep 0.5
}

# Installazione i3 window manager
i3_window_manager (){
    logo
    echo -e "${BCyan}:::   INSTALLAZIONE WINDOW MANAGER   :::${Color_Off}"
    echo
    echo -e "${Green}==> Installazione I3  ...${Color_Off}"
    echo
    sudo dnf install i3-gaps i3-status polybar alacritty xsetroot picom dunst dmenu rofi -y
    echo
    echo -e "${BGreen}+++   Installazione window manager eseguita con successo   +++${Color_Off}"
    sleep 0.5
}

# Installazione shell
fish_shell (){
    logo
    echo -e "${BCyan}:::   INSTALLAZIONE SHELL   :::${Color_Off}"
    echo
    echo -e "${Green}==> Installazione fish shell   ...${Color_Off}"
    echo
    sudo dnf install fish -y
    echo
    echo
    echo -e "${Green}==> Installazione Oh My Fish   ...${Color_Off}"
    echo
    curl -L https://get.oh-my.fish > install
    fish install
    echo
    echo -e "${Green}==> Copia file di configurazione fish shell   ...${Color_Off}"
    echo
    mkdir -p ~/.config/fish
    curl "https://gitlab.com/undermod/dotfile/-/raw/master/fish/config.fish" --output ~/.config/fish/config.fish
    echo
    echo -e "${Green}==> Copia tema Dracula per fish shell   ...${Color_Off}"
    echo
    curl "https://gitlab.com/undermod/dotfile/-/raw/master/fish/dracula.fish" --output ~/.config/fish/conf.d/dracula.fish
    echo
    echo -e "${Green}==> Installazione Starhip theme   ...${Color_Off}"
    echo
    sudo dnf install starship -y
    echo
    echo -e "${Green}==> Attivazione fish come shell predefinita   ...${Color_Off}"
    chsh -s /usr/bin/fish
    echo
    echo -e "${BGreen}+++   Installazione fish shell eseguita con successo   +++${Color_Off}"
    sleep 0.5
}

# Installazione Font
font (){
    logo
    echo -e "${BCyan}:::   INSTALLAZIONE JETBRAINS NERD FONTS   :::${Color_Off}"
    echo
    echo -e "${Green}==> Download fonts   ...${Color_Off}"
    echo
    mkdir -p ~/.fonts
    curl "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.2.2/JetBrainsMono.zip" --output ~/Scasricati/jetbrains.zip
    #curl "https://gitlab.com/undermod/dotfile/-/raw/master/assets/JetBrainsMono.zip" --output ~/Scaricati/jetbrains.zip
    echo
    echo -e "${Green}==> Estrazione fonts   ...${Color_Off}"
    echo
    unzip -q ~/Scaricati/jetbrains.zip -d ~/.fonts
    echo
    echo -e "${Green}==> Aggiornamento cache fonts   ...${Color_Off}"
    echo
    fc-cache -vf
    echo
    echo -e "${Green}==> Rimozione file   ...${Color_Off}"
    echo
    rm ~/Scaricati/jetbrains.zip
    echo
    echo -e "${BGreen}+++   Installazione font eseguita con successo   +++${Color_Off}"
    sleep 0.5
}

# Installazione codec multimediali
codec (){
    logo
    echo -e "${BCyan}:::   INSTALLAZIONE CODEC MULTIMEDIALI   :::${Color_Off}"
    echo
    echo -e "${Green}==> Installazione codec   ...${Color_Off}"
    echo
    sudo dnf install gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel -y
    sudo dnf install lame\* --exclude=lame-devel -y
    echo
    echo -e "${Green}==> Aggiornamento gruppo Multimedia   ...${Color_Off}"
    echo
    sudo dnf group upgrade --with-optional Multimedia -y
    echo
    echo -e "${BGreen}+++   Installazione codec multimediali eseguita con successo   +++${Color_Off}"
    sleep 0.5
}

# Installazione temi
temi (){
    logo
    echo -e "${BCyan}:::   INSTALLAZIONE TEMA, ICONE E CURSORI   :::${Color_Off}"
    echo
    echo -e "${Green}==> Installazione tema GTK  ...${Color_Off}"
    echo
    # TODO: importazione tema gtk in base al DE/WM
    echo
    echo -e "${Green}==> Installazione icone  ...${Color_Off}"
    echo
    # TODO: importazione tema icone in base al DE/WM
    echo
    echo -e "${Green}==> Installazione cursori  ...${Color_Off}"
    echo
    # TODO: importazione tema cursori in base al DE/WM
    echo
    echo -e "${BGreen}+++   Installazione tema,icone e cursori eseguita con successo   +++${Color_Off}"
    sleep 0.5
}

# Importazione configurazioni
dotfile (){
    logo
    echo -e "${BCyan}:::   IMPORTAZIONE DOTFILE UND3RM0D   :::${Color_Off}"
    echo
    echo -e "${Green}==> Importazione configurazione Xresources  ...${Color_Off}"
    echo
    curl "https://gitlab.com/undermod/dotfile/-/raw/master/Xresources/Xresources" --output ~/.Xresources
    echo
    echo -e "${BGreen}+++   Configurazione Xresources importata con successo   +++${Color_Off}"
    echo
    echo
    echo -e "${Green}==> Importazione configurazione Bspwm  ...${Color_Off}"
    echo
    mkdir -p ~/.config/bspwm
    curl "https://gitlab.com/undermod/dotfile/-/raw/master/bspwm/bspwmrc" --output ~/.config/bspwm/bspwmrc
    echo
    echo -e "${BGreen}+++   Configurazione Bspwm importata con successo   +++${Color_Off}"
    echo
    echo -e "${Green}==> Importazione configurazione Sxhkd  ...${Color_Off}"
    echo
    mkdir -p ~/.config/sxhkd
    curl "https://gitlab.com/undermod/dotfile/-/raw/master/bspwm/sxhkdrc" --output ~/.config/sxhkd/sxhkdrc
    echo
    echo -e "${BGreen}+++   Configurazione Sxhkd importata con successo   +++${Color_Off}"
    echo
    echo -e "${Green}==> Importazione configurazione Alacritty  ...${Color_Off}"
    echo
    mkdir -p ~/.config/alacritty
    curl "https://gitlab.com/undermod/dotfile/-/raw/master/alacritty/alacritty.yml" --output ~/.config/alacritty/alacritty.yml
    curl "https://gitlab.com/undermod/dotfile/-/raw/master/alacritty/dracula.yml" --output ~/.config/alacritty/dracula.yml
    echo
    echo -e "${BGreen}+++   Configurazione Alacritty importata con successo   +++${Color_Off}"
    echo
    echo -e "${Green}==> Importazione configurazione Polybar  ...${Color_Off}"
    echo
    mkdir -p ~/.config/polybar/scripts
    curl "https://gitlab.com/undermod/dotfile/-/raw/master/polybar/config-fedora" --output ~/.config/polybar/config
    curl "https://gitlab.com/undermod/dotfile/-/raw/master/polybar/launch.sh" --output ~/.config/polybar/launch.sh
    curl "https://gitlab.com/undermod/dotfile/-/raw/master/polybar/scripts/fedora_updates.sh" --output ~/.config/polybar/scripts/fedora_updates.sh
    curl "https://gitlab.com/undermod/dotfile/-/raw/master/polybar/scripts/fedora_upgrade.sh" --output ~/.config/polybar/scripts/fedora_upgrade.sh
    echo
    echo -e "${BGreen}+++   Configurazione Polybar importata con successo   +++${Color_Off}"
    echo
    echo -e "${Green}==> Importazione configurazione Picom  ...${Color_Off}"
    echo
    mkdir -p ~/.config/picom
    curl "https://gitlab.com/undermod/dotfile/-/raw/master/picom/picom.conf" --output ~/.config/picom/picom.conf
    echo
    echo -e "${BGreen}+++   Configurazione Picom importata con successo   +++${Color_Off}"
    echo
    echo
    echo -e "${Green}==> Importazione configurazione Dunst  ...${Color_Off}"
    echo
    mkdir -p ~/.config/dunst
    curl "https://gitlab.com/undermod/dotfile/-/raw/master/dunst/dunstrc" --output ~/.config/dunst/dunstrc
    echo
    echo -e "${BGreen}+++   Configurazione Dunst importata con successo   +++${Color_Off}"
    echo
    echo
    echo -e "${Green}==> Importazione configurazione Rofi  ...${Color_Off}"
    echo
    mkdir -p ~/.config/rofi
    curl "https://gitlab.com/undermod/dotfile/-/raw/master/rofi/config.rasi" --output ~/.config/rofi/config.rasi
    curl "https://gitlab.com/undermod/dotfile/-/raw/master/rofi/color.rasi" --output ~/.config/rofi/color.rasi
    curl "https://gitlab.com/undermod/dotfile/-/raw/master/rofi/undermod.rasi" --output ~/.config/rofi/undermod.rasi
    echo
    echo -e "${BGreen}+++   Configurazione Rofi importata con successo   +++${Color_Off}"
    echo
}

# --- Procedure di post installazione --- #

procedura_Gnome (){
    tuning_dnf
    update
    check_dipendenze
    font
    codec
    software
}

procedura_Bspwm (){
    tuning_dnf
    update
    user_dirs
    repo
    check_dipendenze
    server_grafico
    display_manager
    bspwm_window_manager
    fish_shell
    font
    codec
    software
    dotfile
    #riavvio
}

procedura_I3 (){
    tuning_dnf
    update
    user_dirs
    repo
    check_dipendenze
    server_grafico
    display_manager
    i3_window_manager
    fish_shell
    font
    codec
    software
    dotfile
    #riavvio
}

# --- Parte principale dello script --- #

disclaimer
riepilogo
echo "Ok hai continuato"
if [[ "$procedura" == "Gnome" ]];
then
    procedura_Gnome
fi
if [[ "$procedura" == "Bspwm" ]];
then
    procedura_Bspwm
fi
if [[ "$procedura" == "I3" ]];
then
    procedura_I3
fi

# Tema Fluent x Gnome
#./install.sh -t default -i fedora --tweaks round --tweaks noborder
